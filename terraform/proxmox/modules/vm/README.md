## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_proxmox"></a> [proxmox](#requirement\_proxmox) | 2.9.14 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_proxmox"></a> [proxmox](#provider\_proxmox) | 2.9.14 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [proxmox_vm_qemu.vm_resource](https://registry.terraform.io/providers/telmate/proxmox/2.9.14/docs/resources/vm_qemu) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_ansible_command"></a> [ansible\_command](#input\_ansible\_command) | `ansible-playbook` command. Path must be relative to the `root.tf` file | `string` | n/a | yes |
| <a name="input_backup_enabled"></a> [backup\_enabled](#input\_backup\_enabled) | Integer value only where: `0` == disabled, and `1` == enabled | `number` | n/a | yes |
| <a name="input_cloud_init_template"></a> [cloud\_init\_template](#input\_cloud\_init\_template) | Available templates as of 01/27/23: `debian11-cloud` | `string` | n/a | yes |
| <a name="input_cpu_cores"></a> [cpu\_cores](#input\_cpu\_cores) | Number of procssor cores. Usually 2x socket number | `number` | n/a | yes |
| <a name="input_cpu_sockets"></a> [cpu\_sockets](#input\_cpu\_sockets) | Number of processor sockets. | `number` | n/a | yes |
| <a name="input_default_gateway"></a> [default\_gateway](#input\_default\_gateway) | Gateway IPv4 | `string` | `"10.10.50.10"` | no |
| <a name="input_default_user"></a> [default\_user](#input\_default\_user) | Default user | `string` | `"shakir"` | no |
| <a name="input_hdd_size"></a> [hdd\_size](#input\_hdd\_size) | Disk size in Gibibytes, e.g. `128G` | `string` | n/a | yes |
| <a name="input_hostname"></a> [hostname](#input\_hostname) | VM hostname | `string` | n/a | yes |
| <a name="input_inventory_path"></a> [inventory\_path](#input\_inventory\_path) | Ansible inventory file, must be relative to the `root.tf` file | `string` | n/a | yes |
| <a name="input_ip_address"></a> [ip\_address](#input\_ip\_address) | Host IPv4. Must be in CIDR notation, e.g. `10.20.30.99/24` | `string` | n/a | yes |
| <a name="input_ip_without_cidr"></a> [ip\_without\_cidr](#input\_ip\_without\_cidr) | Same IPv4 `ip_address` section but without the CIDR notation. Needed for the `local-exec` block | `string` | n/a | yes |
| <a name="input_mac_address"></a> [mac\_address](#input\_mac\_address) | A valid unicast mac address - This is a Pfsense specfic setup | `string` | n/a | yes |
| <a name="input_memory"></a> [memory](#input\_memory) | Memory in MiB (Mebibyte) | `string` | n/a | yes |
| <a name="input_proxmox_host"></a> [proxmox\_host](#input\_proxmox\_host) | Proxmox host IP address | `string` | `"10.10.50.20"` | no |
| <a name="input_pvt_key_path"></a> [pvt\_key\_path](#input\_pvt\_key\_path) | Path to private SSH key for `local-exec` provisioner to run Ansible playbooks | `string` | n/a | yes |
| <a name="input_storage_pool"></a> [storage\_pool](#input\_storage\_pool) | Available pools as of 01/27/23: `local`, `local-lvm`, `ssd-r10` | `string` | n/a | yes |
| <a name="input_tags"></a> [tags](#input\_tags) | Space separated tags | `string` | n/a | yes |
| <a name="input_target_node"></a> [target\_node](#input\_target\_node) | Name of the Proxmox node where the resource will be provisioned | `string` | `"pve"` | no |
| <a name="input_vm_description"></a> [vm\_description](#input\_vm\_description) | Host description | `string` | n/a | yes |

## Outputs

No outputs.
