module "galaxy" {
  source              = "../../modules/vm"
  pvt_key_path        = "/home/shakir/.ssh/id_rsa"
  default_user        = "shakir"
  inventory_path      = "../../../../ansible/inventory.ini"
  hostname            = "galaxy"
  target_node         = "pve1"
  ip_address          = "10.10.50.30/24"
  mac_address         = "f6:3c:c6:ca:14:2b"
  default_gateway     = "10.10.50.10"
  cpu_cores           = 2
  cpu_sockets         = 1
  memory              = "8192"
  hdd_size            = "128G"
  storage_pool        = "sda"
  cloud_init_template = "debian11-cloud"
  vm_description      = "Main selfhosted apps VM"
  tags                = "galaxy prod debian12"
  ansible_command     = "../../../../ansible/galaxy.playbook.yml"
  ip_without_cidr     = "10.10.50.30"
  backup_enabled      = 1
}
