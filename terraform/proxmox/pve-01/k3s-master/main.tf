module "k3s-master" {
  source              = "../../modules/vm"
  pvt_key_path        = "/home/shakir/.ssh/id_rsa"
  default_user        = "shakir"
  inventory_path      = "../../../../ansible/inventory.ini"
  hostname            = "k3s-master"
  target_node         = "pve1"
  ip_address          = "10.10.50.50/24"
  mac_address         = "4e:d8:9f:a9:69:ae"
  default_gateway     = "10.10.50.10"
  cpu_cores           = 2
  cpu_sockets         = 1
  memory              = "2048"
  hdd_size            = "32G"
  storage_pool        = "local-lvm"
  cloud_init_template = "debian11-cloud"
  vm_description      = "k3s control plane"
  tags                = "devops testing debian11 k8s ctrl"
  ansible_command     = "../../../../ansible/k3s.playbook.yml"
  ip_without_cidr     = "10.10.50.50"
  backup_enabled      = 1
}
