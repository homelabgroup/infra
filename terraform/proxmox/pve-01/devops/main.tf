module "devops" {
  source              = "../../modules/vm"
  pvt_key_path        = "/home/shakir/.ssh/id_rsa"
  default_user        = "shakir"
  inventory_path      = "../../../../ansible/inventory.ini"
  hostname            = "devops"
  target_node         = "pve1"
  ip_address          = "10.10.50.33/24"
  mac_address         = "8a:f8:47:47:e2:88"
  default_gateway     = "10.10.50.10"
  cpu_cores           = 2
  cpu_sockets         = 1
  memory              = "2048"
  hdd_size            = "32G"
  storage_pool        = "sdc"
  cloud_init_template = "debian11-cloud"
  vm_description      = "Testing, CI/CD, and build vm"
  tags                = "devops dev debian12"
  ansible_command     = "../../../../ansible/devops.playbook.yml"
  ip_without_cidr     = "10.10.50.33"
  backup_enabled      = 1
}
