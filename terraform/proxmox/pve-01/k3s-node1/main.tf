module "k3s-node1" {
  source              = "../../modules/vm"
  pvt_key_path        = "/home/shakir/.ssh/id_rsa"
  default_user        = "shakir"
  inventory_path      = "../../../../ansible/inventory.ini"
  hostname            = "k3s-node1"
  target_node         = "pve1"
  ip_address          = "10.10.50.51/24"
  mac_address         = "da:57:84:00:d5:31"
  default_gateway     = "10.10.50.10"
  cpu_cores           = 2
  cpu_sockets         = 1
  memory              = "2048"
  hdd_size            = "32G"
  storage_pool        = "local-lvm"
  cloud_init_template = "debian11-cloud"
  vm_description      = "k3s node 1 testing cluster"
  tags                = "devops testing debian11 k8s worker"
  ansible_command     = "&> /dev/null || true"
  ip_without_cidr     = "10.10.50.51"
  backup_enabled      = 1
}
