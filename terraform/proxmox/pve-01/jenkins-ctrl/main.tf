module "jenkins-ctrl" {
  source              = "../../modules/vm"
  pvt_key_path        = "/home/shakir/.ssh/id_rsa"
  default_user        = "shakir"
  inventory_path      = "../../../../ansible/inventory.ini"
  hostname            = "jenkins-ctrl"
  target_node         = "pve1"
  ip_address          = "10.10.50.40/24"
  mac_address         = "3e:84:56:5d:02:bd"
  default_gateway     = "10.10.50.10"
  cpu_cores           = 2
  cpu_sockets         = 1
  memory              = "2048"
  hdd_size            = "24G"
  storage_pool        = "ssd-r10"
  cloud_init_template = "debian11-cloud"
  vm_description      = "Testing CI/CD"
  tags                = "jenkins dev debian11"
  ansible_command     = "../../../../ansible/jenkins.playbook.yml"
  ip_without_cidr     = "10.10.50.40"
  backup_enabled      = 1
}
