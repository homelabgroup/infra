
Provision a new VM (or LXC):
1. Terraform:
    - Create a new module. Use one of the existing modules as an example. The module directory should be named after the resource's name.
    - Add the VM name in `providers.tf` - `terraform.backend.key`. The VM state file on S3 will be named after the VM's name.
    - Add the needed parameters in `main.tf`.
2. Ansible:    
    - Create an Ansible playbook in `infra/ansible`. Ensure the `host` in the playbook matches the `hostname` in the module's `main.tf`.
    - Add the VM's IP in the `inventory.ini` file.

3. In the TF module's directory format the module, initialize the back end, and apply:
```
$ terraform fmt
$ terraform init 
$ terraform plan 
$ terraform apply
```