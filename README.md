
# Deprecation Notice
This project is deprecated. New infrastructure code has been moved to >> https://gitlab.com/homelabgroup/infrastructure

[![pipeline status](https://gitlab.com/homelabgroup/cicd/badges/main/pipeline.svg?ignore_skipped=true)](https://gitlab.com/homelabgroup/cicd/-/commits/main)

# HomeLab Infrastructure

The Terraform and Ansible modules in this repo have been developed to run on a specific infrastructure. Therefore they most likely will not run out of the box on another host without heavy modifications. But I hope they provide some ideas for anyone interested in automating their home-lab resources.

I use [Proxmox as the virtualization environment](https://www.proxmox.com/en/), [pfSense+](https://www.pfsense.org/) on [Netgate 4100](https://shop.netgate.com/products/4100-base-pfsense), [Nginx Proxy Manager](https://nginxproxymanager.com/) for internal reverse proxy and SSL/TLS management, Terraform and Ansible for IaC, and Docker (on VMs) to host the [applications](https://gitlab.com/homelabgroup/apps). I do not use the Proxmox console to create resources (VMs/LXCs). Everything is provisioned as code. I follow the [cattle-not-pets](https://cloudscaling.com/blog/cloud-computing/the-history-of-pets-vs-cattle/) methodology, which helped me immensely reduce infrastructure maintenance.

 Terraform modules in this repository are the starting point for provisioning VMs and LXC containers. TF modules perform the following:
1. Start a new VM using a pre-configured cloud-init template on the Proxmox server.

    - All VMs are provisioned with a hardcoded, static local IPv4 (to maintain a clean Ansible inventory).
    - MAC address are hardcoded. I use [this website](https://www.hellion.org.uk/cgi-bin/randmac.pl) to generate unicast MAC addreesses when I provision a VM. I needed to [set the MAC addresses statically](https://gitlab.com/homelabgroup/infra/-/blob/main/terraform/proxmox/modules/vm/main.tf?ref_type=heads#L40) for hosts on Proxmox because PfSense needs the host's MAC address to assign it a _static DHCP lease_ and ARP binding for it. This way, the hostname can be resolved by PFSense's DNS resolver (I run an internal domain managed by pfSense). In short, I wanted to be able to communicate with the machines using hostnames instead of IP addresses, and this process outlines the necessary steps I had to take.

2. [Triggering Ansible playbook](https://gitlab.com/homelabgroup/infra/-/blob/main/terraform/proxmox/modules/vm/main.tf?ref_type=heads#L71) specific to that VM.

    Depending on the VM's function, Ansible is used for the following tasks:
    - Installing standard packages and configuring various settings for the VM.
    - Setting up a new GitLab runner to integrate the VM with the CI pipeline (on most VMs). This runner is dedicated exclusively to that specific VM and won't pick up any other jobs from the CI. This functionality is implemented through VMs-to-Runners tagging. I use hostnames in the CI job `tags`. And the runner is locked and can't run untagged jobs.
    - Establishing a connection between the VM and network storage (NFS). Given that all applications are containerized using Docker, data is stored on the NFS server.
    
This approach allows for the recycling of VMs/LXCs (terminating and provisioning) without concerns about data loss. For instance, if a VM is sick, I can dragged behind the barn 🐄, shoot it 🔫, and deploy a new one easily ✅ (`terraform destroy` + `terraform apply`). The new VM will automatically connect to the NAS to retrieve application data. This process ensures data integrity and saves time.
