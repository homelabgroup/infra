#!/bin/bash

#cmd=$(ansible-lint --format pep8 -v --show-relpath --force-color  roles/ --exclude roles/datadog.datadog)

print_center() {
  termwidth="$(tput cols)"
  padding="$(printf '%0.1s' -{1..500})"
  printf '%*.*s %s %*.*s\n' 0 "$(((termwidth-2-${#1})/2))" "$padding" "$1" 0 "$(((termwidth-1-${#1})/2))" "$padding"
}

for file in ./*.playbook.yml; do
    
    f=$(basename "$file")
    
    print_center "Linting $(basename "$f")" 
    ansible-lint --format pep8 -v \
                --show-relpath \
                --exclude roles/datadog.datadog \
                --force-color  "$f" 
done
