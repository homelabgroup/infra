---
# https://gitlab-runner-downloads.s3.amazonaws.com/latest/index.html
- name: Download Gitlab Runner
  ansible.builtin.get_url:
    url: "{{ gitlab_deb_pkg_url }}"
    dest: /tmp/{{ gitlab_deb_pkg_name }}
    mode: 0755
    checksum: "sha256:{{ gitlab_deb_pkg_sha }}"

- name: Install Gitlab Runner
  ansible.builtin.apt:
    deb: /tmp/{{ gitlab_deb_pkg_name }}

- name: Ensure gitlab-runner is running
  ansible.builtin.systemd:
    name: gitlab-runner
    state: started
    enabled: true

- name: Add user 'gitlab-runner' to docker group
  ansible.builtin.user:
    name: gitlab-runner
    groups: docker
    append: true

# The following task recycles Gitlab runners (uninstall -> install)
# -----------------
# IMPORTANT:
# See these Gitlab deprications:
# - registration-tokens & gitlab-runner register command:
#     https://gitlab.com/gitlab-org/gitlab/-/issues/380872
# -----------------

- name: Purge all available Gitlab runners
  ansible.builtin.script: "{{ role_path }}/files/unregister-runners.py --token={{ gitlab_api_token }} --hostname={{ inventory_hostname }}"
  register: exec_result

- name: Print script stdout
  ansible.builtin.debug:
    var: exec_result.stdout

- name: Register Gitlab Runner (system-mode)
  when: registration_token | default('', true) | trim != ''
  register: create
  loop: "{{ list_of_runners }}"
  # --tag-list -> comma separated tags
  ansible.builtin.shell: |
    gitlab-runner register \
    --description "ansible-generated on {{ inventory_hostname }}" \
    --non-interactive \
    --run-untagged="false" \
    --locked="false" \
    --access-level="not_protected" \
    --url "https://gitlab.com" \
    --registration-token "{{ registration_token }}" \
    --executor shell \
    --tag-list="{{ inventory_hostname }}" \

# Print this message if the gitlab-ci registration token was not exported as env var
- name: Print msg
  when: create.changed == false
  ansible.builtin.debug:
    msg:
      - => Registration token variable 'CI_TOKEN' is not set. No runner was registered.
      - => export CI_TOKEN=<yourToken> before executing this role.

- name: Delet Gitlab Runner dep package
  ansible.builtin.file:
    path: /tmp/{{ gitlab_deb_pkg_name }}
    state: absent
