Role Name
=========

Gitlab Runner

A role to download, install, and configure Gitlab Runner instances on servers. The role also will add the gitlab-runner user to the docker group to allow executing docker commands on the host.

Requirements
------------

- Ensure that the necessary dependencies for Gitlab Runner are met on the target servers.
- Docker group.

Role Variables
--------------

The role requires the following variables to be defined:
- `gitlab_deb_pkg_url`: The URL to download the Gitlab Runner Debian package.
- `gitlab_deb_pkg_name`: The name of the Gitlab Runner Debian package.
- `gitlab_deb_pkg_sha`: The SHA256 checksum of the Gitlab Runner Debian package.
- `gitlab_api_token`: The Gitlab API token used for registering runners exported as an environment variable in the current shell.
- `runner_description`: Description for the registered Gitlab Runner.
- `list_of_runners`: List of runners to register.
- `registration_token`: Registration token for Gitlab Runner exported as an environment variable in the current shell.

Dependencies
------------

No dependencies required.

Example Playbook
----------------

Including an example of how to use the role:

    - hosts: servers
      roles:
         - gitlab-runner
